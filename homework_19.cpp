#include <iostream>

class Pokemon 
{
public:
    virtual void Voice() const = 0;
};

class Charmander: public Pokemon
{
public:
    void Voice() const override
    {
        std::cout << "Char-Char!\n";
    }
};

class Squirtle: public Pokemon
{
public:
    void Voice() const override
    {
        std::cout << "Squirt!\n";
    }
};

class Bulbasaur: public Pokemon
{
public:
    void Voice() const override
    {
        std::cout << "Bulb-Bulb-Bulb!\n";
    }
};

class Pikachu: public Pokemon
{
public:
    void Voice() const override
    {
        std::cout << "PIKAAAAA-CHUUUUUUUU!\n";
    }
};

int main()
{
    Pokemon* pokemons[4];
    pokemons[0] = new Charmander();
    pokemons[1] = new Squirtle();
    pokemons[2] = new Bulbasaur();
    pokemons[3] = new Pikachu();

    for (Pokemon* a:pokemons)
        a->Voice();
}

